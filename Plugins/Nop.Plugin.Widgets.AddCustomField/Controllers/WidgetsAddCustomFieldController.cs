﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Widgets.AddCustomField.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc.Filters;

namespace Nop.Plugin.Widgets.AddCustomField.Controllers
{
    [Area(AreaNames.Admin)]
    [AuthorizeAdmin]
    [AutoValidateAntiforgeryToken]
    public class WidgetsAddCustomFieldController : BasePluginController
    {

        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;



        public WidgetsAddCustomFieldController(
            ILocalizationService localizationService,
            INotificationService notificationService,
            IPermissionService permissionService,
            ISettingService settingService,
            IStoreContext storeContext)
        {
            _localizationService = localizationService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _settingService = settingService;
            _storeContext = storeContext;
        }

        
        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var AddCustomFieldSettings = _settingService.LoadSetting<AddCustomFieldSettings>(storeScope);

            var model = new ConfigurationModel
            {
                Input_Name = AddCustomFieldSettings.Input_Name,
            };

            if (storeScope > 0)
            {
                model.Input_Name_OverrideForStore = _settingService.SettingExists(AddCustomFieldSettings, x => x.Input_Name, storeScope);
            }

            return View("~/Plugins/Widgets.AddCustomField/Views/Configure.cshtml", model);
        }

        [HttpPost]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var AddCustomFieldSettings = _settingService.LoadSetting<AddCustomFieldSettings>(storeScope);

            AddCustomFieldSettings.Input_Name = model.Input_Name;

            _settingService.SaveSettingOverridablePerStore(AddCustomFieldSettings, x => x.Input_Name, model.Input_Name_OverrideForStore, storeScope, false);

            _settingService.ClearCache();

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));

            return Configure();
        }

    }
}