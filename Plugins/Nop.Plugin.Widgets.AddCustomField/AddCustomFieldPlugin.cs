﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Plugins;
using Nop.Web.Framework.Infrastructure;

namespace Nop.Plugin.Widgets.AddCustomField
{
    /// <summary>
    /// Google Analytics plugin
    /// </summary>
    public class AddCustomFieldPlugin : BasePlugin
    {
        private readonly ILocalizationService _localizationService;
        private readonly IWebHelper _webHelper;
        private readonly ISettingService _settingService;

        public AddCustomFieldPlugin(IWebHelper webHelper, ISettingService settingService)
        {
            _webHelper = webHelper;
            _settingService = settingService;
        }

        public override string GetConfigurationPageUrl()
        {
            return _webHelper.GetStoreLocation() + "Admin/WidgetsAddCustomField/Configure";
        }
        public override void Install()
        { 
            base.Install();
        }
        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<AddCustomFieldSettings>();

            //locales
            _localizationService.DeletePluginLocaleResources("Plugins.Widgets.AddCustomField");

            base.Uninstall();
        }
        public bool HideInWidgetList => false;
    }
}