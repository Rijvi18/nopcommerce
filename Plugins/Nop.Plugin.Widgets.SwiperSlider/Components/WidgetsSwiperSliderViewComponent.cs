﻿using System;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Plugin.Widgets.SwiperSlider.Infrastructure.Cache;
using Nop.Plugin.Widgets.SwiperSlider.Models;
using Nop.Services.Caching;
using Nop.Services.Configuration;
using Nop.Services.Media;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Widgets.SwiperSlider.Components
{
    [ViewComponent(Name = "WidgetsSwiperSlider")]
    public class WidgetsSwiperSliderViewComponent : NopViewComponent
    {
        private readonly ICacheKeyService _cacheKeyService;
        private readonly IStoreContext _storeContext;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly ISettingService _settingService;
        private readonly IPictureService _pictureService;
        private readonly IWebHelper _webHelper;

        public WidgetsSwiperSliderViewComponent(ICacheKeyService cacheKeyService,
            IStoreContext storeContext,
            IStaticCacheManager staticCacheManager,
            ISettingService settingService,
            IPictureService pictureService,
            IWebHelper webHelper)
        {
            _cacheKeyService = cacheKeyService;
            _storeContext = storeContext;
            _staticCacheManager = staticCacheManager;
            _settingService = settingService;
            _pictureService = pictureService;
            _webHelper = webHelper;
        }

        public IViewComponentResult Invoke(string widgetZone, object additionalData)
        {
            var SwiperSliderSettings = _settingService.LoadSetting<SwiperSliderSettings>(_storeContext.CurrentStore.Id);

            var model = new PublicInfoModel
            {
                Picture1Url = GetPictureUrl(SwiperSliderSettings.Picture1Id),
                Text1 = SwiperSliderSettings.Text1,
                Link1 = SwiperSliderSettings.Link1,
                AltText1 = SwiperSliderSettings.AltText1,

                Picture2Url = GetPictureUrl(SwiperSliderSettings.Picture2Id),
                Text2 = SwiperSliderSettings.Text2,
                Link2 = SwiperSliderSettings.Link2,
                AltText2 = SwiperSliderSettings.AltText2,

                Picture3Url = GetPictureUrl(SwiperSliderSettings.Picture3Id),
                Text3 = SwiperSliderSettings.Text3,
                Link3 = SwiperSliderSettings.Link3,
                AltText3 = SwiperSliderSettings.AltText3,

                Picture4Url = GetPictureUrl(SwiperSliderSettings.Picture4Id),
                Text4 = SwiperSliderSettings.Text4,
                Link4 = SwiperSliderSettings.Link4,
                AltText4 = SwiperSliderSettings.AltText4,

                Picture5Url = GetPictureUrl(SwiperSliderSettings.Picture5Id),
                Text5 = SwiperSliderSettings.Text5,
                Link5 = SwiperSliderSettings.Link5,
                AltText5 = SwiperSliderSettings.AltText5
            };

            if (string.IsNullOrEmpty(model.Picture1Url) && string.IsNullOrEmpty(model.Picture2Url) &&
                string.IsNullOrEmpty(model.Picture3Url) && string.IsNullOrEmpty(model.Picture4Url) &&
                string.IsNullOrEmpty(model.Picture5Url))
                //no pictures uploaded
                return Content("");

            return View("~/Plugins/Widgets.SwiperSlider/Views/PublicInfo.cshtml", model);
        }

        protected string GetPictureUrl(int pictureId)
        {
            var cacheKey = _cacheKeyService.PrepareKeyForDefaultCache(ModelCacheEventConsumer.PICTURE_URL_MODEL_KEY,
                pictureId, _webHelper.IsCurrentConnectionSecured() ? Uri.UriSchemeHttps : Uri.UriSchemeHttp);

            return _staticCacheManager.Get(cacheKey, () =>
            {
                //little hack here. nulls aren't cacheable so set it to ""
                var url = _pictureService.GetPictureUrl(pictureId, showDefaultPicture: false) ?? "";
                return url;
            });
        }
    }
}
