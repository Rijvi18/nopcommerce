﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Widgets.SwiperSlider.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Widgets.SwiperSlider.Controllers
{
    [Area(AreaNames.Admin)]
    [AutoValidateAntiforgeryToken]
    public class WidgetsSwiperSliderController : BasePluginController
    {
        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IPictureService _pictureService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;

        public WidgetsSwiperSliderController(ILocalizationService localizationService,
            INotificationService notificationService,
            IPermissionService permissionService,
            IPictureService pictureService,
            ISettingService settingService,
            IStoreContext storeContext)
        {
            _localizationService = localizationService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _pictureService = pictureService;
            _settingService = settingService;
            _storeContext = storeContext;
        }

        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var SwiperSliderSettings = _settingService.LoadSetting<SwiperSliderSettings>(storeScope);
            var model = new ConfigurationModel
            {
                Picture1Id = SwiperSliderSettings.Picture1Id,
                Text1 = SwiperSliderSettings.Text1,
                Link1 = SwiperSliderSettings.Link1,
                AltText1 = SwiperSliderSettings.AltText1,
                Picture2Id = SwiperSliderSettings.Picture2Id,
                Text2 = SwiperSliderSettings.Text2,
                Link2 = SwiperSliderSettings.Link2,
                AltText2 = SwiperSliderSettings.AltText2,
                Picture3Id = SwiperSliderSettings.Picture3Id,
                Text3 = SwiperSliderSettings.Text3,
                Link3 = SwiperSliderSettings.Link3,
                AltText3 = SwiperSliderSettings.AltText3,
                Picture4Id = SwiperSliderSettings.Picture4Id,
                Text4 = SwiperSliderSettings.Text4,
                Link4 = SwiperSliderSettings.Link4,
                AltText4 = SwiperSliderSettings.AltText4,
                Picture5Id = SwiperSliderSettings.Picture5Id,
                Text5 = SwiperSliderSettings.Text5,
                Link5 = SwiperSliderSettings.Link5,
                AltText5 = SwiperSliderSettings.AltText5,
                ActiveStoreScopeConfiguration = storeScope
            };

            if (storeScope > 0)
            {
                model.Picture1Id_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.Picture1Id, storeScope);
                model.Text1_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.Text1, storeScope);
                model.Link1_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.Link1, storeScope);
                model.AltText1_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.AltText1, storeScope);
                model.Picture2Id_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.Picture2Id, storeScope);
                model.Text2_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.Text2, storeScope);
                model.Link2_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.Link2, storeScope);
                model.AltText2_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.AltText2, storeScope);
                model.Picture3Id_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.Picture3Id, storeScope);
                model.Text3_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.Text3, storeScope);
                model.Link3_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.Link3, storeScope);
                model.AltText3_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.AltText3, storeScope);
                model.Picture4Id_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.Picture4Id, storeScope);
                model.Text4_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.Text4, storeScope);
                model.Link4_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.Link4, storeScope);
                model.AltText4_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.AltText4, storeScope);
                model.Picture5Id_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.Picture5Id, storeScope);
                model.Text5_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.Text5, storeScope);
                model.Link5_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.Link5, storeScope);
                model.AltText5_OverrideForStore = _settingService.SettingExists(SwiperSliderSettings, x => x.AltText5, storeScope);
            }

            return View("~/Plugins/Widgets.SwiperSlider/Views/Configure.cshtml", model);
        }

        [HttpPost]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var SwiperSliderSettings = _settingService.LoadSetting<SwiperSliderSettings>(storeScope);

            //get previous picture identifiers
            var previousPictureIds = new[]
            {
                SwiperSliderSettings.Picture1Id,
                SwiperSliderSettings.Picture2Id,
                SwiperSliderSettings.Picture3Id,
                SwiperSliderSettings.Picture4Id,
                SwiperSliderSettings.Picture5Id
            };

            SwiperSliderSettings.Picture1Id = model.Picture1Id;
            SwiperSliderSettings.Text1 = model.Text1;
            SwiperSliderSettings.Link1 = model.Link1;
            SwiperSliderSettings.AltText1 = model.AltText1;
            SwiperSliderSettings.Picture2Id = model.Picture2Id;
            SwiperSliderSettings.Text2 = model.Text2;
            SwiperSliderSettings.Link2 = model.Link2;
            SwiperSliderSettings.AltText2 = model.AltText2;
            SwiperSliderSettings.Picture3Id = model.Picture3Id;
            SwiperSliderSettings.Text3 = model.Text3;
            SwiperSliderSettings.Link3 = model.Link3;
            SwiperSliderSettings.AltText3 = model.AltText3;
            SwiperSliderSettings.Picture4Id = model.Picture4Id;
            SwiperSliderSettings.Text4 = model.Text4;
            SwiperSliderSettings.Link4 = model.Link4;
            SwiperSliderSettings.AltText4 = model.AltText4;
            SwiperSliderSettings.Picture5Id = model.Picture5Id;
            SwiperSliderSettings.Text5 = model.Text5;
            SwiperSliderSettings.Link5 = model.Link5;
            SwiperSliderSettings.AltText5 = model.AltText5;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.Picture1Id, model.Picture1Id_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.Text1, model.Text1_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.Link1, model.Link1_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.AltText1, model.AltText1_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.Picture2Id, model.Picture2Id_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.Text2, model.Text2_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.Link2, model.Link2_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.AltText2, model.AltText2_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.Picture3Id, model.Picture3Id_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.Text3, model.Text3_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.Link3, model.Link3_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.AltText3, model.AltText3_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.Picture4Id, model.Picture4Id_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.Text4, model.Text4_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.Link4, model.Link4_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.AltText4, model.AltText4_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.Picture5Id, model.Picture5Id_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.Text5, model.Text5_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.Link5, model.Link5_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(SwiperSliderSettings, x => x.AltText5, model.AltText5_OverrideForStore, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            //get current picture identifiers
            var currentPictureIds = new[]
            {
                SwiperSliderSettings.Picture1Id,
                SwiperSliderSettings.Picture2Id,
                SwiperSliderSettings.Picture3Id,
                SwiperSliderSettings.Picture4Id,
                SwiperSliderSettings.Picture5Id
            };

            //delete an old picture (if deleted or updated)
            foreach (var pictureId in previousPictureIds.Except(currentPictureIds))
            {
                var previousPicture = _pictureService.GetPictureById(pictureId);
                if (previousPicture != null)
                    _pictureService.DeletePicture(previousPicture);
            }

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));
            return Configure();
        }
    }
}
