﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Widgets.EditorBoxPlugin.Models;
using Nop.Services.Configuration;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Widgets.EditorBoxPlugin.Components
{
    [ViewComponent(Name = "WidgetsEditorBoxPlugin")]
    public class WidgetsEditorBoxPluginViewComponent : NopViewComponent
    {
        public static string ViewComponentName => "WidgetsEditorBoxPlugin";
        private readonly IStoreContext _storeContext;
        private readonly ISettingService _settingService;

        public WidgetsEditorBoxPluginViewComponent(IStoreContext storeContext, ISettingService settingService)
        {
            _storeContext = storeContext;
            _settingService = settingService;
        }

        public IViewComponentResult Invoke(string widgetZone, object additionalData)
        {
            var EditorBoxPluginSettings = _settingService.LoadSetting<EditorBoxPluginSettings>(_storeContext.CurrentStore.Id);

            var model = new PublicInfoModel
            {
                Message = EditorBoxPluginSettings.Message,
                UseSandbox = EditorBoxPluginSettings.UseSandbox
            };

            if (!model.UseSandbox && string.IsNullOrEmpty(model.Message))
                return Content("");

            return View("~/Plugins/Widgets.EditorBoxPlugin/Views/PublicInfo.cshtml", model);
        }
    }
}