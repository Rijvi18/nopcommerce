﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Widgets.EditorBoxPlugin
{
    class EditorBoxPluginSettings : ISettings
    {
        public bool UseSandbox { get; set; }
        public string Message { get; set; }
    }
}
