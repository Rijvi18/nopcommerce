﻿using Nop.Core;
using Nop.Plugin.Widgets.EditorBoxPlugin.Components;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Plugins;
using Nop.Web.Framework.Infrastructure;
using System.Collections.Generic;

namespace Nop.Plugin.Widgets.EditorBoxPlugin
{
    public class EditorBoxPlugin : BasePlugin, IWidgetPlugin
    {
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly ILocalizationService _localizationService;

        public bool HideInWidgetList =>false;

        public EditorBoxPlugin(ISettingService settingService, IWebHelper webHelper, ILocalizationService localizationService)
        {
            _webHelper = webHelper;
            _settingService = settingService;
            _localizationService = localizationService;
        }
        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/EditorBoxPlugin/Configure";
        }
        public IList<string> GetWidgetZones()
        {
            return new List<string> { PublicWidgetZones.HomepageTop };
        }

        public string GetWidgetViewComponentName(string widgetZone)
        {
            return WidgetsEditorBoxPluginViewComponent.ViewComponentName;
        }
        public override void Install()
        {
            base.Install();
        }

        public override void Uninstall()
        {
            _localizationService.DeletePluginLocaleResource("Plugin.Widgets.EditorBoxPlugin.UseSandbox");
            _localizationService.DeletePluginLocaleResource("Plugin.Widgets.EditorBoxPlugin.Message");
            base.Uninstall();
        }

    }
}