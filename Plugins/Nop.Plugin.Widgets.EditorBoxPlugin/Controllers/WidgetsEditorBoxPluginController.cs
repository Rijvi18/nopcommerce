﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Widgets.EditorBoxPlugin.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Widgets.EditorBoxPlugin.Controllers
{
    [Area(AreaNames.Admin)]
    public class WidgetsEditorBoxPluginController : BasePluginController
    {
        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;

        public WidgetsEditorBoxPluginController(ILocalizationService localizationService,
            INotificationService notificationService,
            IPermissionService permissionService,
            ISettingService settingService,
            IStoreContext storeContext)
        {
            _localizationService = localizationService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _settingService = settingService;
            _storeContext = storeContext;
        }

        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var EditorBoxPluginSettings = _settingService.LoadSetting<EditorBoxPluginSettings>(storeScope);
            var model = new ConfigurationModel
            {
                Message = EditorBoxPluginSettings.Message,
                UseSandbox = EditorBoxPluginSettings.UseSandbox
            };

            return View("~/Plugins/Widgets.EditorBoxPlugin/Views/Configure.cshtml", model);
        }

        [HttpPost]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var EditorBoxPluginSettings = _settingService.LoadSetting<EditorBoxPluginSettings>(storeScope);

            EditorBoxPluginSettings.Message = model.Message;
            EditorBoxPluginSettings.UseSandbox = model.UseSandbox;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSetting(EditorBoxPluginSettings, x => x.Message, storeScope, false);
            _settingService.SaveSetting(EditorBoxPluginSettings, x => x.UseSandbox, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));
            return Configure();
        }
    }
}