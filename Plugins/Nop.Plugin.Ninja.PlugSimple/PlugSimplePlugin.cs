﻿using Nop.Services.Cms;
using Nop.Services.Plugins;
using Nop.Web.Framework.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Ninja.PlugSimple
{
    class PlugSimplePlugin:BasePlugin,IWidgetPlugin
    {

        public bool HideInWidgetList => false;

        public string GetWidgetViewComponentName(string widgetZone)
        {
           if(widgetZone.Equals(PublicWidgetZones.HeaderLinksBefore))
            {
                return "CustomHeader";
            }
            else if(widgetZone.Equals(PublicWidgetZones.HomepageBeforeProducts))
            {
                return "NinjaSimple";
            }
           else if (widgetZone.Equals(PublicWidgetZones.HeaderMenuBefore))
            {
                return "CustomCategory";
            }
            else
            {
                return "";
            }
        }
        public IList<string> GetWidgetZones()
        {
            return new List<string> { PublicWidgetZones.HomepageBeforeProducts,PublicWidgetZones.HeaderLinksBefore, PublicWidgetZones.HeaderMenuBefore};
        }
        public override void Install()
        {
            base.Install();
        }                                            
        public override void Uninstall()
        {
            base.Uninstall();
        }
    }
}
