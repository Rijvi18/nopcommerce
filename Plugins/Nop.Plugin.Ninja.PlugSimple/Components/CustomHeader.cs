﻿using Microsoft.AspNetCore.Mvc;
using Nop.Web.Framework.Components;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Ninja.PlugSimple.Components
{
    public class CustomHeaderViewComponent: NopViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View("~/Plugins/Ninja.PlugSimple/Views/customheader.cshtml");
        }
    }
}
