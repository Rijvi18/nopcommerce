﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Plugin.Widgets.CustomSlider.Infrastructure.Cache;
using Nop.Plugin.Widgets.CustomSlider.Models;
using Nop.Services.Caching;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Widgets.CustomSlider.Controllers
{
    [Area(AreaNames.Admin)]
    [AutoValidateAntiforgeryToken]
    public class WidgetsCustomSliderController : BasePluginController
    {
        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly IWebHelper _webHelper;
        private readonly ICacheKeyService _cacheKeyService;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly IPictureService _pictureService;

        public WidgetsCustomSliderController(ILocalizationService localizationService,
            INotificationService notificationService,
            IPermissionService permissionService, 
            ISettingService settingService,
            IStoreContext storeContext, IWebHelper webHelper, ICacheKeyService cacheKeyService, IStaticCacheManager staticCacheManager, IPictureService pictureService)
        {
            _localizationService = localizationService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _settingService = settingService;
            _storeContext = storeContext;
            _webHelper = webHelper;
            _cacheKeyService = cacheKeyService;
            _pictureService = pictureService;
            _staticCacheManager = staticCacheManager;
        }

        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var CustomSliderSettings = _settingService.LoadSetting<CustomSliderSettings>(storeScope);

            string[] num = CustomSliderSettings.PictureIds.Split(',');


            var model = new ConfigurationModel();
            for(int i = 0; i < num.Count(); i++)
            {
                model.PictureUrls.Add(GetPictureUrl(short.Parse(num[i])));

                
            }


            return View("~/Plugins/Widgets.CustomSlider/Views/Configure.cshtml", model);
        }

        [HttpPost]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var CustomSliderSettings = _settingService.LoadSetting<CustomSliderSettings>(storeScope);

            //get previous picture identifiers

            if (string.IsNullOrEmpty(CustomSliderSettings.PictureIds) && string.IsNullOrEmpty(CustomSliderSettings.Texts) && string.IsNullOrEmpty(CustomSliderSettings.Links) && string.IsNullOrEmpty(CustomSliderSettings.AltTexts))
            {
                CustomSliderSettings.PictureIds +=model.PictureId;
                CustomSliderSettings.Texts += model.Text;
                CustomSliderSettings.Links += model.Link;
                CustomSliderSettings.AltTexts += model.AltText;
            }
            else
            {
                CustomSliderSettings.PictureIds += "," + model.PictureId;
                CustomSliderSettings.Texts += "," + model.Text;
                CustomSliderSettings.Links += "," + model.Link;
                CustomSliderSettings.AltTexts += "," + model.AltText;

            }


            _settingService.SaveSettingOverridablePerStore(CustomSliderSettings, x => x.PictureIds, false, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(CustomSliderSettings, x => x.Texts, false, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(CustomSliderSettings, x => x.Links, false, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(CustomSliderSettings, x => x.AltTexts, false, storeScope, false);

            _settingService.ClearCache();

            //get current picture identifiers

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));
            return Configure();
        }

        protected string GetPictureUrl(int pictureId)
        {
            var cacheKey = _cacheKeyService.PrepareKeyForDefaultCache(ModelCacheEventConsumer.PICTURE_URL_MODEL_KEY,
                pictureId, _webHelper.IsCurrentConnectionSecured() ? Uri.UriSchemeHttps : Uri.UriSchemeHttp);

            return _staticCacheManager.Get(cacheKey, () =>
            {
                //little hack here. nulls aren't cacheable so set it to ""
                var url = _pictureService.GetPictureUrl(pictureId, showDefaultPicture: false) ?? "";
                return url;
            });
        }

    }
}