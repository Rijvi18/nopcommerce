﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.CustomSlider.Models
{
    public class PublicInfoModel : BaseNopModel
    {
        public string PictureUrls { get; set; }
        public string Texts { get; set; }
        public string Links { get; set; }
        public string AltTexts { get; set; }

    }
}