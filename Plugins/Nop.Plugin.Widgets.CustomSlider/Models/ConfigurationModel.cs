﻿using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Plugin.Widgets.CustomSlider.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        public ConfigurationModel()
        {
            PictureUrls = new List<string>();
        }
        [NopResourceDisplayName("Plugins.Widgets.CustomSlider.Picture")]
        [UIHint("Picture")]

        public int PictureId { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.CustomSlider.Text")]
        public string Text { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.CustomSlider.Link")]
        public string Link { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.CustomSlider.AltText")]
        public string AltText { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.CustomSlider.List")]
        public List<string> PictureUrls { get; set; } 




    }
}