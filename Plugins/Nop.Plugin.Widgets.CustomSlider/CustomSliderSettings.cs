﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Widgets.CustomSlider
{
    public class CustomSliderSettings : ISettings
    {
        public string PictureIds { get; set; } //"1,2,3,4,5,6,7"
        public string Texts { get; set; }
        public string Links { get; set; }
        public string AltTexts { get; set; }


    }
}